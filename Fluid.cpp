//
//  Fluid.cpp
//  mySketch
//
//  Created by Michael Sedbon on 27/11/2016.
//
//

#include "Fluid.h"

void Fluid::setup(){
    Parameters.setName("Fluid Controls");
    
    Parameters.add(lineWidth.set("lineWidth", 5, 0, 10));
    
    
    OutputSyphonServer.setName("Fluid Server");
    
    
    left.assign(bufferSize, 0.0);
    right.assign(bufferSize, 0.0);
    mainVolume.assign(bufferSize, 0.0);
    

    
    
}

void Fluid::draw(){
    

    
    ofPushStyle();
    ofPushMatrix();
    
    ofBackground(0);// draw grey background
    ofSetColor(255);// set color drawing color to white
    ofTranslate(ofGetWidth()/2,ofGetHeight()/2); // move initital drawing postion to the center of the screen
    int circle_resolution=1600; // amount of points circle is made of, more points - better quality, but could decrease perfomance
    int radius=200;
    int smoothing_amount=10;
    
    ofPolyline circle_sin;
    ofPolyline circle_cos;
    ofPolyline circle_un_cos;
    
    
    int wave_height=radius*0.2;
    int anim_shape=8;
    float sine_pct=0.2; // setting in percantage how big is the part
    
    float speed_increment=ofGetElapsedTimef()*0.2;
    
    int line_w=lineWidth;
    int radius_cos=radius+line_w-1; // let's make it little less then line width, so circles will overlap each other
    int radius_un_cos=radius+(line_w-1)*2;
    for(int i=0; i<circle_resolution; i++)
    {
        
        float angle=TWO_PI/circle_resolution*i;
        float raidus_addon=wave_height*sin((angle+speed_increment)*anim_shape);
        
        float x=cos(angle+speed_increment)*radius;
        float y=sin(angle+speed_increment)*radius;
        
        // drawing sine wave only on a part of the circle
        if(i<sine_pct*circle_resolution)
        {
            x=cos(angle+speed_increment)*(radius+raidus_addon);
            y=sin(angle+speed_increment)*(radius+raidus_addon);
        }
        circle_sin.addVertex(ofPoint(x,y));
        
        
        raidus_addon=wave_height*cos((angle+speed_increment)*anim_shape);
        x=cos(angle+speed_increment)*(radius_cos);
        y=sin(angle+speed_increment)*(radius_cos);
        
        if(i<sine_pct*circle_resolution)
        {
            x=cos(angle+speed_increment)*(radius_cos+raidus_addon);
            y=sin(angle+speed_increment)*(radius_cos+raidus_addon);
        }
        
        circle_cos.addVertex(ofPoint(x,y));
        
        
        //increment a wave angle to flip the wave
        raidus_addon=wave_height*cos((angle+TWO_PI/3+speed_increment)*anim_shape);
        x=cos(angle+speed_increment)*(radius_un_cos);
        y=sin(angle+speed_increment)*(radius_un_cos);
        
        if(i<sine_pct*circle_resolution)
        {
            x=cos(angle+speed_increment)*(radius_un_cos+raidus_addon);
            y=sin(angle+speed_increment)*(radius_un_cos+raidus_addon);
        }
        
        circle_un_cos.addVertex(ofPoint(x,y));
        
    }
    
    ofSetLineWidth(line_w);
    
     clr1 = freshYellow.getLerped(orangeFire, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
    ofSetColor(clr1);
    circle_sin.close(); // to connect first and last point of our shape we need to use ‘close’ function
    circle_sin= circle_sin.getSmoothed(smoothing_amount);
    circle_sin.draw();
    
     clr2= orangeFire.getLerped(freshYellow, ofMap(sin(ofGetElapsedTimef()*0.7),-1.,1.,0.,1.));
    ofSetColor(clr2);
    circle_cos.close();
    circle_cos= circle_cos.getSmoothed(smoothing_amount);
    circle_cos.draw();
    
    
     clr3=blancCasse.getLerped(fullWhite, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
    ofSetColor(clr3);
    circle_un_cos.close();
    circle_un_cos= circle_un_cos.getSmoothed(smoothing_amount);
    circle_un_cos.draw();

    
    ofPopMatrix();
    ofPopStyle();
   
}

void Fluid::audioReceived(float * input, int bufferSize, int nChannels) {
    // Scale entre 0 et 1
    float maxValue = 0;
    for (int i = 0; i < bufferSize; i++) {
        if (abs(input[i]) > maxValue) {
            maxValue = abs(input[i]);
        }
    }
    for (int i = 0; i < bufferSize; i++) {
        input[i] /= maxValue;
    }
    
    
    // Obtention de mainVolume smoothed et lissage
    for (int i = 0; i < bufferSize; i++) {
        left[i]		= input[i*nChannels]*0.5;
        right[i]	= input[i*nChannels+1]*0.5;
        
        mainVolume[i] = mainVolume[i]*0.98;
        mainVolume[i] += 0.02*(left[i] + right[i]);
        

    }
    

    
    for (int k = 0; k<4; k++) {
        for (int i = 1; i < bufferSize-1; i++) {
            mainVolume[i] = (mainVolume[i-1] + mainVolume[i] + mainVolume[i+1])/3;
        }
    }
}
