//
//  Circles.h
//  mySketch
//
//  Created by Taf on 24/11/2016.
//
//

#ifndef __mySketch__Circles__
#define __mySketch__Circles__

#include "ofMain.h"
#include "ofxSyphon.h"

#define MAX_NB_CIRCLES 50

class Circles {
public:
    void setup();
    void draw();
    
    ofParameterGroup Parameters;
    ofParameter<int> nbMaxCircles, initRadius, finalRadius, fadeDistance, radialSpeed;
    
    ofxSyphonServer OutputSyphonServer;
    
    int nbCircles;
    int radiuses[MAX_NB_CIRCLES];
    
    float lowAvg, threshold;
    bool above;
    
    int nbColorBands, storedNbColorBands;
    vector<ofColor> palette, setColors;
    ofColor fullWhite = ofColor::fromHex(0xFFFFFF);
    int colorMode, storedColorMode, alpha, storedAlpha, lineWidth;
};

#endif /* defined(__mySketch__Circles__) */
