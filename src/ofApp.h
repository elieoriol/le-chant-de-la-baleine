#pragma once

#include "ofMain.h"
#include "ofxSyphon.h"
#include "ofxGui.h"
#include "ofxParameterMidiSync.h"

#include "WaveForm.h"
#include "Signals.h"
#include "UpDown.h"
#include "SignalsUpDown.h"
#include "Fluid.h"
#include "GradientCircle.h"
#include "MovingCircle.h"
#include "LineFluid.h"
#include "Circles.h"
#include "Ikeda.h"

#define NB_PALETTES 10

class ofApp : public ofBaseApp{
    
public:
    
    void setup();
    void draw();
    void audioReceived(float * input, int bufferSize, int nChannels = 10);
    
    void keyPressed(int key);
    
    int bufferSize = 256;
    ofSoundStream soundStream;
    
    ofxSyphonClient mClient;
    
    ofxPanel masterGui, waveFormGui, signalGui, upDownGui, signalUpDownGui, fluidGui, movingCircleGui, lineFluidGui, circlesGui;
    ofParameterGroup allParameters;
    ofParameterGroup master, fftMaster, aspectMaster, signalMaster;
    ofParameter<bool> gradientBackground;
    ofParameter<int> bandDivide;
    ofParameter<int> fftBand;
    ofParameter<float> threshold;
    ofParameter<int> alpha, lineWidth, colorMode, nbColorBands;
    ofParameter<int> mode, nbr, height;
    bool hideGUI;
    bool lock;
    ofxPanel inputDeviceGui;
    ofParameterGroup inputDevice;
    ofParameter<int> soundDeviceId, midiDevice1Id, midiDevice2Id;
    int storedSoundDeviceId, storedMidiDevice1Id, storedMidiDevice2Id;
    bool soundDeviceList;
    bool midiDeviceList;
    
    ofxParameterMidiSync sync1, sync2;
    bool setMidi1, setMidi2;
    
    WaveForm waveForm;
    Signals signal;
    UpDown upDown;
    SignalsUpDown signalUpDown;
    Fluid fluid;
    GradientCircle gradientCircle;
    MovingCircle movingCircle;
    LineFluid lineFluid;
    Circles circles;
    Ikeda ikeda;
    
    

    int caseKey;
        
    ofxFft* fft;
    vector <float> audioBins;
    
    
    //----------Global --------
    ofColor blancCasse = ofColor::fromHex(0xECE9E6);
    ofColor fullWhite = ofColor::fromHex(0xFFFFFF);
    ofColor lightBlack = ofColor::fromHex(0x2B2B2B);
    
    //------------------BLEUs-----------------------------------
    
    //----------Palette 1  blueNuances --------
    ofColor pastelBlue1 = ofColor::fromHex(0x84BCCB);
    ofColor pastelBlue2 = ofColor::fromHex(0x2C6D7F);
    ofColor pastelBlue3 = ofColor::fromHex(0x1A414C);
    
    //----------Palette 2  blue red skin--------
    ofColor aquaBlue = ofColor::fromHex(0x47AFCB);
    ofColor heetchRed = ofColor::fromHex(0xE6324B);
    ofColor skinColor = ofColor::fromHex(0xf2E3C6);
    
    //----------Palette 3  blue red skin--------
    ofColor denseBlue = ofColor::fromHex(0x1E84C4);
    //blancCasse
    //light Black
    
    //----------Palette 4  Darker Blue Nuances--------
    ofColor sadBlue = ofColor::fromHex(0x2C6D7F);
    ofColor deepBlue = ofColor::fromHex(0x202E53);
    //blancCasse
    
    //------------------RED SKIN--------------------------
    
    
    //----------Palette 1 nazi Skin--------
    
    // heetchRed
    //blancCasse
    //lightBlack
    
    //----------Palette 2 50 shades of skin--------
    
    //skinColor
    ofColor rossbeefSkin = ofColor::fromHex(0xFFC6A5);
    //heetchRed
    
    //------------------Vert Pastel--------------------------
    
    ofColor pastelGreen = ofColor::fromHex(0xB1C4B0);
    ofColor darkPastelGreen = ofColor::fromHex(0x6B8584);
    ofColor darkPastelRed = ofColor::fromHex(0xBB414E);
    
    //------------------Neon--------------------------
    
    ofColor neonGreen = ofColor::fromHex(0xB4DEC1);
    ofColor neonRed = ofColor::fromHex(0xFF1F4C);
    ofColor offNeonRed = ofColor::fromHex(0xA85163);
    
    
    
    ofColor clr1, clr2, clr3, clr4;
    
    vector<ofColor> palettes[NB_PALETTES];
    
    int NB_LINES = 5;
    
};
