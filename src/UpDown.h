//
//  UpDown.h
//  mySketch
//
//  Created by Taf on 24/11/2016.
//
//

#ifndef __mySketch__UpDown__
#define __mySketch__UpDown__

#include "ofMain.h"
#include "ofxSyphon.h"
#include "ofxFft.h"

#define MAX_PTS 500
#define FCTN_PTS 10

class UpDown {
public:
    void setup();
    void draw();
    
    ofParameterGroup Parameters;
    int mode, nbr, height;
    
    ofxSyphonServer OutputSyphonServer;
    
    int bufferSize = 256;
    
    ofPoint pts[MAX_PTS];
    int nPts;
    int storedMode, storedNbr;
    bool created;
    
    int it;
    
    int dsc;
    bool move;
    
    float lowAvg, threshold;
    
    int nbColorBands, storedNbColorBands;
    vector<ofColor> palette, setColors;
    ofColor fullWhite = ofColor::fromHex(0xFFFFFF);
    int colorMode, storedColorMode, alpha, storedAlpha, lineWidth;
};

#endif /* defined(__mySketch__UpDown__) */
