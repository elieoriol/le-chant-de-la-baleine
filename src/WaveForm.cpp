//
//  WaveForm.cpp
//  mySketch
//
//  Created by Taf on 23/11/2016.
//
//

#include "WaveForm.h"

void WaveForm::setup() {
    Parameters.setName("WaveForm");
    
    Parameters.add(ampValue.set("ampValue", 2500, 0, 20000));
    Parameters.add(tangents.set("tangents", false));
    Parameters.add(nbTangents.set("Nb Tangents", 5000, 0, 10000));
    Parameters.add(normals.set("normals", false));
    Parameters.add(nbNormals.set("Nb Normals", 5000, 0, 10000));
    Parameters.add(normalLength.set("normalLength", 50, 0, 300));
    
    
    OutputSyphonServer.setName("WaveForm Server");
    
    
    left.assign(bufferSize, 0.0);
    right.assign(bufferSize, 0.0);
    mainVolume.assign(bufferSize, 0.0);
    
    nbColorBands = 3;
    polylines.resize(nbColorBands);
    palette.resize(3);
    setColors.resize(nbColorBands);
    storedNbColorBands = nbColorBands;
    colorMode = 0;
    storedColorMode = colorMode;
    alpha = 255;
    storedAlpha = alpha;
    lineWidth = 5;
}



void WaveForm::draw() {
    ofNoFill();
    ofPushStyle();
    ofPushMatrix();
    ofTranslate(0, ofGetHeight()/2, 0);
    ofEnableAlphaBlending();
    
    if (storedAlpha != alpha) {
        storedAlpha = alpha;
        for (int i = 0; i < setColors.size(); i++) {
            setColors[i].a = alpha;
        }
        fullWhite.a = alpha;
    }
    
    if (storedNbColorBands != nbColorBands or storedColorMode != colorMode) {
        for (int n = 0; n < polylines.size(); n++) polylines[n].clear();
        polylines.resize(nbColorBands);
        setColors.resize(nbColorBands);
        storedNbColorBands = nbColorBands;
        
        for (int n = 0; n < setColors.size(); n++) {
            setColors[n] = palette[(int) ofRandom(0, palette.size())];
            setColors[n].a = alpha;
        }
        storedColorMode = colorMode;
    }
    
    
    for (int n = 0; n < polylines.size(); n++) {
        ofSetLineWidth(lineWidth);
        ofSetColor(fullWhite.getLerped(setColors[n], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
        polylines[n].clear();
        for (unsigned int i = n*mainVolume.size()/nbColorBands; i <= (n+1)*mainVolume.size()/nbColorBands + 5; i++){
            if (i*4 >= ofGetWidth()) break;
            polylines[n].curveTo(i*4, -mainVolume[i]*ampValue);
        }
        polylines[n].draw();
        
        if (tangents) {
            ofSetLineWidth(1);
            float realNbTangents = nbTangents / nbColorBands;
            for (int p=0; p<realNbTangents; p+=1) {
                ofVec3f point = polylines[n].getPointAtPercent((float) p/realNbTangents);
                float floatIndex = polylines[n].getIndexAtPercent((float) p/realNbTangents);
                ofVec3f tangent = polylines[n].getTangentAtIndexInterpolated(floatIndex) * 50;
                ofDrawLine(point-tangent/2, point+tangent/2);
            }
        }
        
        if (normals) {
            float realNbNormals = nbNormals / nbColorBands;
            ofSetLineWidth(1);
            for (int p=0; p<realNbNormals; p+=1) {
                ofVec3f point = polylines[n].getPointAtPercent((float) p/realNbNormals);
                float floatIndex = polylines[n].getIndexAtPercent((float) p/realNbNormals);
                ofVec3f normal = polylines[n].getNormalAtIndexInterpolated(floatIndex) * normalLength;
                ofDrawLine(point-normal/2, point+normal/2);
            }
        }
    }
        
    
    ofDisableAlphaBlending();
    ofPopMatrix();
    ofPopStyle();
}



void WaveForm::audioReceived(float * input, int bufferSize, int nChannels) {
    
    // Obtention de mainVolume smoothed et lissage
    for (int i = 0; i < bufferSize; i++) {
        left[i]		= input[i*nChannels]*0.5;
        right[i]	= input[i*nChannels+1]*0.5;
        
        mainVolume[i] = mainVolume[i]*0.98;
        mainVolume[i] += 0.02*(left[i] + right[i]);
    }
    
    for (int k = 0; k<10; k++) {
        for (int i = 2; i < bufferSize-2; i++) {
            mainVolume[i] = (mainVolume[i-1] + mainVolume[i] + mainVolume[i+1])/3;
        }
    }
}
