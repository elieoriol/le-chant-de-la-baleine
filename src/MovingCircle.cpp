//
//  MovingCircle.c
//  mySketch
//
//  Created by Michael Sedbon on 27/11/2016.
//
//

#include "MovingCircle.h"

void MovingCircle::setup(){
    Parameters.setName("Moving Circle");
    
    Parameters.add(nbCircles.set("nbCircles", 1, 0, 10));
    Parameters.add(radius.set("radius", 100, 0, 300));
    Parameters.add(colorStyle.set("colorStyle",0,0,9));
    Parameters.add(pourcent.set("pourcent",0.2,0.0,1.0));
    Parameters.add(speedCoef.set("speedCoef", 1., 0.0, 10.));
    Parameters.add(heightCoef.set("heightCoef",0.2,0.0,0.9));
    Parameters.add(waveNumber.set("waveNumber", 2, 0, 15));
    Parameters.add(simple.set("Simple Circle", true));
    Parameters.add(coeff.set("coeff", 5, 0, 10));
    Parameters.add(clampLevel.set("clampLevel", 5, 1, 20));
    Parameters.add(fill.set("fill", false));
    Parameters.add(nbNoiseCircles.set("nbNoiseCircles", 0, 0, 10));
    Parameters.add(lowMapLevel.set("lowMapLevel", 40, -100, 100));
    Parameters.add(highMapLevel.set("highMapLevel", 100, -200, 200));
    
    
    
    OutputSyphonServer.setName("Moving Circle Server");
    
    for (int i = 0; i < MAX_CIRCLES; i++) {
        centers[i].x = ofGetWidth()/2;
        centers[i].y = ofGetHeight()/2;
        moveCenters[i].x = 0;
        moveCenters[i].y = 0;
    }
    above = false;
    sgn = 1;
    
    coefNoiseCircles = 100;
    
    storedNbNoiseCircles = nbNoiseCircles;
    generated = false;
}


void MovingCircle::draw(){
    
    
    switch (colorStyle) {
            
        case 0:
            clr1 = pastelBlue1.getLerped(blancCasse, ofMap(sin(ofGetElapsedTimef()+500),-1.,1.,0.,1.));
            clr2= blancCasse.getLerped(pastelBlue2, ofMap(sin(ofGetElapsedTimef()*0.7),-1.,1.,0.,1.));
            clr3=pastelBlue2.getLerped(blancCasse, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));

            break;
            
        case 1:
            clr1 = aquaBlue.getLerped(fullWhite, ofMap(sin(ofGetElapsedTimef()+500),-1.,1.,0.,1.));
            clr2= blancCasse.getLerped(skinColor, ofMap(sin(ofGetElapsedTimef()*0.7),-1.,1.,0.,1.));
            clr3=aquaBlue.getLerped(fullWhite, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            
            break;
            
        case 2:
            clr1 = heetchRed.getLerped(fullWhite, ofMap(sin(ofGetElapsedTimef()+500),-1.,1.,0.,1.));
            clr2= blancCasse.getLerped(fullWhite, ofMap(sin(ofGetElapsedTimef()*0.7),-1.,1.,0.,1.));
            clr3=aquaBlue.getLerped(fullWhite, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            
            break;
            
        case 3:
            clr1 = blancCasse.getLerped(fullWhite, ofMap(sin(ofGetElapsedTimef()+500),-1.,1.,0.,1.));
            clr2= heetchRed.getLerped(heetchRed, ofMap(sin(ofGetElapsedTimef()*0.7),-1.,1.,0.,1.));
            clr3=fullWhite.getLerped(blancCasse, ofMap(sin(ofGetElapsedTimef()-200),-1.,1.,0.,1.));
            
            break;
            
        case 4:
            clr1 = blancCasse.getLerped(fullWhite, ofMap(sin(ofGetElapsedTimef()+500),-1.,1.,0.,1.));
            clr2= heetchRed.getLerped(heetchRed, ofMap(sin(ofGetElapsedTimef()*0.7),-1.,1.,0.,1.));
            clr3=fullWhite.getLerped(lightBlack, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            
            break;
            
        case 5:
            clr1 = pastelGreen.getLerped(blancCasse, ofMap(sin(ofGetElapsedTimef()+500),-1.,1.,0.,1.));
            clr2= fullWhite.getLerped(darkPastelGreen, ofMap(sin(ofGetElapsedTimef()*0.7),-1.,1.,0.,1.));
            clr3=fullWhite.getLerped(lightBlack, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            
            break;
            
        case 6:
            clr1 = pastelGreen.getLerped(neonGreen, ofMap(sin(ofGetElapsedTimef()+500),-1.,1.,0.,1.));
            clr2= fullWhite.getLerped(darkPastelGreen, ofMap(sin(ofGetElapsedTimef()*0.7),-1.,1.,0.,1.));
            clr3=fullWhite.getLerped(lightBlack, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            
            break;
            
        case 7:
            clr1 = neonRed.getLerped(offNeonRed, ofMap(sin(ofGetElapsedTimef()+500),-1.,1.,0.,1.));
            clr2= fullWhite.getLerped(offNeonRed, ofMap(sin(ofGetElapsedTimef()*0.7),-1.,1.,0.,1.));
            clr3=fullWhite.getLerped(darkPastelGreen, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            
            break;
            
        case 8:
            clr1 = neonGreen.getLerped(neonRed, ofMap(sin(ofGetElapsedTimef()+500),-1.,1.,0.,1.));
            clr2= fullWhite.getLerped(offNeonRed, ofMap(sin(ofGetElapsedTimef()*0.7),-1.,1.,0.,1.));
            clr3=fullWhite.getLerped(lightBlack, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            
            break;
            
        default:
            break;
    }

    
    //D�placement du/des cercle(s)
    for (int i = 0; i < nbCircles; i++) {
        //R�action selon lowAvg
        sgn = 1;
        if (lowAvg > threshold) {
            if (moveCenters[i].x <= 0) sgn = -1;
            moveCenters[i].x = sgn * ((int) ofRandom(1, clampLevel));
            if (moveCenters[i].y > 0) sgn = 1;
            moveCenters[i].y = sgn * ((int) ofRandom(1, clampLevel));
            if (!above) {
                if (ofRandom(-1., 1.) <= 0.) moveCenters[i].x = moveCenters[i].x * (-1);
                if (ofRandom(-1., 1.) <= 0.) moveCenters[i].y = moveCenters[i].y * (-1);
            }
            above = true;
        } else above = false;
        
        //Rester dans l'�cran
        if (!simple) {
            if (centers[i].x + moveCenters[i].x > ofGetWidth() - radius * (1 + heightCoef) or centers[i].x + moveCenters[i].x < radius * (1 + heightCoef)) {
                moveCenters[i].x = (-1) * moveCenters[i].x;
            }
            centers[i].x += moveCenters[i].x;
            if (centers[i].y + moveCenters[i].y > ofGetHeight() - radius * (1 + heightCoef) or centers[i].y + moveCenters[i].y < radius * (1 + heightCoef)) {
                moveCenters[i].y = (-1) * moveCenters[i].y;
            }
            centers[i].y += moveCenters[i].y;
        } else if (simple and nbNoiseCircles > 0) {
            if (centers[i].x + moveCenters[i].x > ofGetWidth() - radius - highMapLevel or centers[i].x + moveCenters[i].x < radius + highMapLevel) moveCenters[i].x = (-1) * moveCenters[i].x;
            centers[i].x += moveCenters[i].x;
            if (centers[i].y + moveCenters[i].y > ofGetHeight() - radius - highMapLevel or centers[i].y + moveCenters[i].y < radius + highMapLevel) moveCenters[i].y = (-1) * moveCenters[i].y;
            centers[i].y += moveCenters[i].y;
        } else {
            if (centers[i].x + moveCenters[i].x > ofGetWidth() - radius or centers[i].x + moveCenters[i].x < radius) moveCenters[i].x = (-1) * moveCenters[i].x;
            centers[i].x += moveCenters[i].x;
            if (centers[i].y + moveCenters[i].y > ofGetHeight() - radius or centers[i].y + moveCenters[i].y < radius) moveCenters[i].y = (-1) * moveCenters[i].y;
            centers[i].y += moveCenters[i].y;
        }
    }
    
    if (storedNbNoiseCircles != nbNoiseCircles or storedColorStyle != colorStyle) {
        generated = false;
        if (storedNbNoiseCircles != nbNoiseCircles) {
            for (int i = nbNoiseCircles; i < MAX_CIRCLES; i++) {
                noiseCircleColors[i] = ofColor::black;
            }
            storedNbNoiseCircles = nbNoiseCircles;
        } else {
            for (int i = 0; i < MAX_CIRCLES; i++) {
                noiseCircleColors[i] = ofColor::black;
            }
        }
        storedColorStyle = colorStyle;
    }
    
    if (!generated) {
        if (noiseCircleColors[0] == ofColor::black) noiseCircleColors[0] = fullWhite;
        for (int i = 1; i< nbNoiseCircles; i++) {
            if (noiseCircleColors[i] == ofColor::black) {
                int numColor = ofRandom(1., 4.);
                if (numColor == 1) noiseCircleColors[i] = clr1;
                else if (numColor == 2) noiseCircleColors[i] = clr2;
                else noiseCircleColors[i] = clr3;
                noiseCircleColors[i].a = alpha;
            }
        }
        generated = true;
    }
    
    ofEnableAlphaBlending();
    
    if (storedAlpha != alpha) {
        storedAlpha = alpha;
        for (int i = 0; i < nbNoiseCircles; i++) {
            noiseCircleColors[i].a = alpha;
        }
        clr1.a = alpha;
        clr2.a = alpha;
        clr3.a = alpha;
        fullWhite.a = alpha;
    }
    
    
    //Motif simple ou complexe
    if (simple) {
        if(fill) ofFill();
        else ofNoFill();
    
        ofSetLineWidth(lineWidth);
        for (int i = 0; i < nbCircles; i++) {
            if (nbNoiseCircles > 0) {
                for (int j = 0; j < nbNoiseCircles; j++){
                    ofSetColor(noiseCircleColors[j]);
                    ofDrawCircle(centers[i].x + ofMap(sin((ofGetElapsedTimef()*3.5) + (j*10)),-1,1, lowMapLevel, highMapLevel) , centers[i].y + ofMap(cos((ofGetElapsedTimef()*3) + (j*10)),-1,1, lowMapLevel, highMapLevel), radius);
                }
            }
        
            else {
                ofSetColor(255, 255, 255, alpha);
                ofDrawCircle(centers[i].x, centers[i].y, radius);
            }
            
        }
    } else {
        for (int i = 0; i < nbCircles; i++) {
            drawFluidCircles(centers[i].x, centers[i].y);
        }
    }
    
    ofDisableAlphaBlending();
    
}
    
    
void MovingCircle::drawFluidCircles(float centerX, float centerY){
    ofPushStyle();
    ofPushMatrix();
    
    int circle_resolution = 1600;
    int smoothing_amount=10;
    
    ofPolyline circle_sin;
    ofPolyline circle_cos;
    ofPolyline circle_un_cos;
    
    
    int wave_height=radius*heightCoef;
    int anim_shape=waveNumber;
    float sine_pct=pourcent; // setting in percantage how big is the part
    
    float speed_increment=ofGetElapsedTimef()*speedCoef;
    
    int line_w=lineWidth;
    int radius_cos=radius+line_w-1; // let's make it little less then line width, so circles will overlap each other
    int radius_un_cos=radius+(line_w-1)*2;
    
    
    for(int i=0; i<circle_resolution; i++)
    {
        
        float angle=TWO_PI/circle_resolution*i;
        float raidus_addon=wave_height*sin((angle+speed_increment)*anim_shape);
        
        float x=centerX + cos(angle+speed_increment)*radius;
        float y=centerY + sin(angle+speed_increment)*radius;
        
        // drawing sine wave only on a part of the circle
        if(i<sine_pct*circle_resolution)
        {
            x=centerX + cos(angle+speed_increment)*(radius+raidus_addon);
            y=centerY + sin(angle+speed_increment)*(radius+raidus_addon);
        }
        circle_sin.addVertex(ofPoint(x,y));
        
        
        raidus_addon=wave_height*cos((angle+speed_increment)*anim_shape);
        x=centerX+cos(angle+speed_increment)*(radius_cos);
        y=centerY+sin(angle+speed_increment)*(radius_cos);
        
        if(i<sine_pct*circle_resolution)
        {
            x=centerX+cos(angle+speed_increment)*(radius_cos+raidus_addon);
            y=centerY+sin(angle+speed_increment)*(radius_cos+raidus_addon);
        }
        
        circle_cos.addVertex(ofPoint(x,y));
        
        
        //increment a wave angle to flip the wave
        raidus_addon=wave_height*cos((angle+TWO_PI/3+speed_increment)*anim_shape);
        x=centerX+cos(angle+speed_increment)*(radius_un_cos);
        y=centerY+sin(angle+speed_increment)*(radius_un_cos);
        
        if(i<sine_pct*circle_resolution)
        {
            x=centerX+cos(angle+speed_increment)*(radius_un_cos+raidus_addon);
            y=centerY+sin(angle+speed_increment)*(radius_un_cos+raidus_addon);
        }
        
        circle_un_cos.addVertex(ofPoint(x,y));
        
    }
    
    
    
    
    
    
    
    ofSetLineWidth(line_w);
    
    ofSetColor(clr1);
    circle_sin.close(); // to connect first and last point of our shape we need to use �close� function
    circle_sin= circle_sin.getSmoothed(smoothing_amount);
    circle_sin.draw();
    
    ofSetColor(clr2);
    circle_cos.close();
    circle_cos= circle_cos.getSmoothed(smoothing_amount);
    circle_cos.draw();
    
    ofSetColor(clr3);
    circle_un_cos.close();
    circle_un_cos= circle_un_cos.getSmoothed(smoothing_amount);
    circle_un_cos.draw();
    
    
    
    ofPopMatrix();
    ofPopStyle();
}
