//
//  UpDown.cpp
//  mySketch
//
//  Created by Taf on 24/11/2016.
//
//

#include "UpDown.h"

void UpDown::setup() {
    Parameters.setName("UpDown");
    
    
    OutputSyphonServer.setName("UpDown Server");
    
    
    nPts = 0;
    created = false;
    
    
    it = FCTN_PTS;
    
    dsc = 1;
    move = false;
    
    lowAvg = 0.;
    threshold = 0.;
    
    nbColorBands = 3;
    palette.resize(3);
    setColors.resize(nbColorBands);
    storedNbColorBands = nbColorBands;
    colorMode = 0;
    storedColorMode = colorMode;
    alpha = 255;
    storedAlpha = alpha;
    lineWidth = 5;
}


void UpDown::draw() {
    ofTranslate(0, ofGetHeight()/2, 0);
    ofEnableAlphaBlending();
    
    if (storedAlpha != alpha) {
        storedAlpha = alpha;
        for (int i = 0; i < setColors.size(); i++) {
            setColors[i].a = alpha;
        }
        fullWhite.a = alpha;
    }
    
    if (storedNbColorBands != nbColorBands or storedColorMode != colorMode) {
        setColors.resize(nbColorBands);
        storedNbColorBands = nbColorBands;
        
        for (int n = 0; n < setColors.size(); n++) {
            setColors[n] = palette[(int) ofRandom(0, palette.size())];
            setColors[n].a = alpha;
        }
        storedColorMode = colorMode;
    }
    
    switch (mode) {
        case 1:
            nPts = nbr*4;
            break;
            
        default:
            nPts = nbr*2;
            break;
    }
    
    if(storedMode != mode or storedNbr != nbr) {
        storedMode = mode;
        storedNbr = nbr;
        created = false;
    }
    
    if (!created) {
        switch (mode) {
            case 1:
                for (int i = 0; i < nPts; i+=4) {
                    pts[i].x = i*256/nbr;
                    pts[i].y = -height/2;
                    pts[i+1].x = (i+2)*256/nbr;
                    pts[i+1].y = -height/2;
                    pts[i+2].x = (i+2)*256/nbr;
                    pts[i+2].y = height/2;
                    pts[i+3].x = (i+4)*256/nbr;
                    pts[i+3].y = height/2;
                }
                break;
                
            case 2:
                for (int i = 0; i < nPts; i+=2) {
                    pts[i].x = i*512/nbr;
                    pts[i].y = height/2;
                    pts[i+1].x = (i+1)*512/nbr;
                    pts[i+1].y = -height/2;
                }
                break;
                
            case 3:
                for (int i = 0; i < nPts; i+=2) {
                    pts[i].x = i*512/nbr;
                    pts[i].y = height/2;
                    pts[i+1].x = (i+2)*512/nbr;
                    pts[i+1].y = -height/2;
                }
                break;
        }
        
        for (int i = nPts; i < MAX_PTS; i++) {
            pts[i].x = 0;
            pts[i].y = 0;
        }
        
        if (mode == 2) {
            pts[nPts].x = 1024;
            pts[nPts].y = height/2;
        }
        created = true;
        move = false;
        dsc = 1;
        it = FCTN_PTS;
    }
    
    
    if (lowAvg > threshold) {
        move = true;
    }
    
    //ofDrawBitmapString(lowAvg, 700, 300);
    
    
    if (move) {
        switch (mode) {
            case 1:
                for (int i = 0; i < nPts; i+=4) {
                    pts[i].y = dsc*(-height/2)*it/FCTN_PTS;
                    pts[i+1].y = pts[i].y;
                    pts[i+2].y = -pts[i].y;
                    pts[i+3].y = -pts[i].y;
                }
                break;
                
            default:
                for (int i = 0; i < nPts; i+=2) {
                    pts[i].y = dsc*(-height/2)*it/FCTN_PTS;
                    pts[i+1].y = -pts[i].y;
                }
                break;
        }
        
        if (mode == 2) {
            pts[nPts].y = dsc*(-height/2)*it/FCTN_PTS;
        }
        it--;
    }
    
    if (it==-FCTN_PTS) {
        move = false;
        it = FCTN_PTS;
        dsc = dsc*(-1);
    }
    
    
    ofSetLineWidth(lineWidth);
    
    int k = 0, c = 0;
    
    for (int i = 0; i < nPts-1; i++) {
        if (c++ == (int) nPts/nbColorBands - 1) {
            ofSetColor(fullWhite.getLerped(setColors[k++], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
            c = 0;
        }
        else ofSetColor(fullWhite.getLerped(setColors[k], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
        ofDrawLine(pts[i].x, pts[i].y, pts[i+1].x, pts[i+1].y);
    }
    if (mode == 2) ofDrawLine(pts[nPts-1].x, pts[nPts-1].y, pts[nPts].x, pts[nPts].y);
    
    ofDisableAlphaBlending();
    
    ofTranslate(0, -ofGetHeight()/2, 0);
}
