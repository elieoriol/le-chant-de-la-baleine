//
//  MovingCircle.h
//  mySketch
//
//  Created by Michael Sedbon on 27/11/2016.
//
//

#ifndef MovingCircle_h
#define MovingCircle_h

#include "ofMain.h"
#include "ofxSyphon.h"
#include "ofxFft.h"

#define MAX_CIRCLES 10


class MovingCircle {
public:
    void setup();
    void draw();
    void drawFluidCircles(float x, float y);
    
    ofParameterGroup Parameters;
    ofParameter<int> nbCircles;
    ofParameter<int> radius;
    ofParameter<int> coeff;
    ofParameter<int> clampLevel;
    ofParameter <int> colorStyle;
    ofParameter <float> pourcent;
    ofParameter <float> speedCoef;
    ofParameter<float> heightCoef;
    ofParameter<int> waveNumber;
    ofParameter<bool> simple;
    ofParameter<bool> fill;
    ofParameter<int> nbNoiseCircles;
    ofParameter<int> lowMapLevel;
    ofParameter<int> highMapLevel;
    int lineWidth, alpha;
    
    ofxSyphonServer OutputSyphonServer;
    
    ofPoint centers[MAX_CIRCLES], moveCenters[MAX_CIRCLES];
    ofColor noiseCircleColors[MAX_CIRCLES];
    int storedNbNoiseCircles, storedColorStyle, storedAlpha;
    bool generated;
    
    int sgn;
    bool above;
    int coefNoiseCircles;
    
    float lowAvg, threshold;
    
    
    //----------Global --------
    ofColor blancCasse = ofColor::fromHex(0xECE9E6);
    ofColor fullWhite = ofColor::fromHex(0xFFFFFF);
    ofColor lightBlack = ofColor::fromHex(0x2B2B2B);
    
    //------------------BLEUs-----------------------------------
    
    //----------Palette 1  blueNuances --------
    ofColor pastelBlue1 = ofColor::fromHex(0x84BCCB);
    ofColor pastelBlue2 = ofColor::fromHex(0x2C6D7F);
    ofColor pastelBlue3 = ofColor::fromHex(0x1A414C);
    
    //----------Palette 2  blue red skin--------
    ofColor aquaBlue = ofColor::fromHex(0x47AFCB);
    ofColor heetchRed = ofColor::fromHex(0xE6324B);
    ofColor skinColor = ofColor::fromHex(0xf2E3C6);
    
    //----------Palette 3  blue red skin--------
    ofColor denseBlue = ofColor::fromHex(0x1E84C4);
    //blancCasse
    //light Black
    
    //----------Palette 4  Darker Blue Nuances--------
    ofColor sadBlue = ofColor::fromHex(0x2C6D7F);
    ofColor deepBlue = ofColor::fromHex(0x202E53);
    //blancCasse
    
    //------------------RED SKIN--------------------------
    
    
    //----------Palette 1 nazi Skin--------
    
    // heetchRed
    //blancCasse
    //lightBlack
    
    //----------Palette 2 50 shades of skin--------
    
    //skinColor
    ofColor rossbeefSkin = ofColor::fromHex(0xFFC6A5);
    //heetchRed
    //lightBlack
    
    //------------------Vert Pastel--------------------------
    
    ofColor pastelGreen = ofColor::fromHex(0xB1C4B0);
    ofColor darkPastelGreen = ofColor::fromHex(0x6B8584);
    ofColor darkPastelRed = ofColor::fromHex(0xBB414E);
    
    //------------------Neon--------------------------
    
    ofColor neonGreen = ofColor::fromHex(0xB4DEC1);
    ofColor neonRed = ofColor::fromHex(0xFF1F4C);
    ofColor offNeonRed = ofColor::fromHex(0xA85163);
    
    
    
    ofColor clr1, clr2, clr3, clr4, clr5, clr6, clr7, clr8;
};


#endif /* MovingCircle_h */
