#include "ofMain.h"
#include "ofApp.h"
#include "ofAppGlutWindow.h"

//========================================================================
int main( ){
    //ofAppGlutWindow window;
	//ofSetupOpenGL(&window,1024,768,OF_WINDOW);			// <-------- setup the GL context
    
    ofGLFWWindowSettings settings;
    settings.width = 1024;
    settings.height = 768;
    settings.resizable = false;
    ofCreateWindow(settings);

	// this kicks off the running of my app
	// can be OF_WINDOW or OF_FULLSCREEN
	// pass in width and height too:
	ofRunApp(new ofApp());
}
