//
//  WaveForm.h
//  mySketch
//
//  Created by Taf on 23/11/2016.
//
//

#ifndef __mySketch__WaveForm__
#define __mySketch__WaveForm__

#include "ofMain.h"
#include "ofxSyphon.h"

class WaveForm {
public:
    void setup();
    void draw();
    void audioReceived(float * input, int bufferSize, int nChannels = 2);
    
    ofParameterGroup Parameters;
    ofParameter<int> ampValue;
    ofParameter<bool> tangents;
    ofParameter<int> nbTangents;
    ofParameter<bool> normals;
    ofParameter<int> nbNormals;
    ofParameter<int> normalLength;
    
    ofxSyphonServer OutputSyphonServer;
    
    int bufferSize = 256;
    
    vector <float> left, right, mainVolume;
    
    vector<ofPolyline> polylines;
    int nbColorBands, storedNbColorBands;
    
    vector<ofColor> palette, setColors;
    ofColor fullWhite = ofColor::fromHex(0xFFFFFF);
    int colorMode, storedColorMode, alpha, storedAlpha, lineWidth;
};

#endif /* defined(__mySketch__WaveForm__) */
