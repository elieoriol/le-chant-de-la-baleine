//
//  Fluid.cpp
//  mySketch
//
//  Created by Michael Sedbon on 27/11/2016.
//
//

#include "Fluid.h"

void Fluid::setup(){
    Parameters.setName("Fluid");
    
    Parameters.add(colorStyle.set("colorStyle", 0, 0, 9));
    Parameters.add(pourcent.set("pourcent", 0.2, 0.0, 1.0));
    Parameters.add(speedCoef.set("speedCoef", 1., 0.0, 10.));
    Parameters.add(heightCoef.set("heightCoef", 0.2, 0.0, 0.9));
    Parameters.add(waveNumber.set("waveNumber", 2, 0, 15));
    
    
    OutputSyphonServer.setName("Fluid Server");
}

void Fluid::draw(){
    ofPushStyle();
    ofPushMatrix();
    
    ofBackground(0);// draw grey background
    ofTranslate(ofGetWidth()/2,ofGetHeight()/2); // move initital drawing postion to the center of the screen
    int circle_resolution=1600; // amount of points circle is made of, more points - better quality, but could decrease perfomance
    int radius=200;
    int smoothing_amount=10;
    
    ofPolyline circle_sin;
    ofPolyline circle_cos;
    ofPolyline circle_un_cos;
    
    
    int wave_height=radius*heightCoef;
    int anim_shape=waveNumber;
    float sine_pct=pourcent; // setting in percantage how big is the part
    
    float speed_increment=ofGetElapsedTimef()*speedCoef;
    
    int line_w=lineWidth;
    int radius_cos=radius+line_w-1; // let's make it little less then line width, so circles will overlap each other
    int radius_un_cos=radius+(line_w-1)*2;
    for(int i=0; i<circle_resolution; i++)
    {
        
        float angle=TWO_PI/circle_resolution*i;
        float raidus_addon=wave_height*sin((angle+speed_increment)*anim_shape);
        
        float x=cos(angle+speed_increment)*radius;
        float y=sin(angle+speed_increment)*radius;
        
        // drawing sine wave only on a part of the circle
        if(i<sine_pct*circle_resolution)
        {
            x=cos(angle+speed_increment)*(radius+raidus_addon);
            y=sin(angle+speed_increment)*(radius+raidus_addon);
        }
        circle_sin.addVertex(ofPoint(x,y));
        
        
        raidus_addon=wave_height*cos((angle+speed_increment)*anim_shape);
        x=cos(angle+speed_increment)*(radius_cos);
        y=sin(angle+speed_increment)*(radius_cos);
        
        if(i<sine_pct*circle_resolution)
        {
            x=cos(angle+speed_increment)*(radius_cos+raidus_addon);
            y=sin(angle+speed_increment)*(radius_cos+raidus_addon);
        }
        
        circle_cos.addVertex(ofPoint(x,y));
        
        
        //increment a wave angle to flip the wave
        raidus_addon=wave_height*cos((angle+TWO_PI/3+speed_increment)*anim_shape);
        x=cos(angle+speed_increment)*(radius_un_cos);
        y=sin(angle+speed_increment)*(radius_un_cos);
        
        if(i<sine_pct*circle_resolution)
        {
            x=cos(angle+speed_increment)*(radius_un_cos+raidus_addon);
            y=sin(angle+speed_increment)*(radius_un_cos+raidus_addon);
        }
        
        circle_un_cos.addVertex(ofPoint(x,y));
        
    }
    
    ofSetLineWidth(line_w);
    
    switch (colorStyle) {
        case 0:
            clr1 = pastelBlue1.getLerped(blancCasse, ofMap(sin(ofGetElapsedTimef()+500),-1.,1.,0.,1.));
            clr2= blancCasse.getLerped(pastelBlue2, ofMap(sin(ofGetElapsedTimef()*0.7),-1.,1.,0.,1.));
            clr3=pastelBlue2.getLerped(blancCasse, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            break;
            
        case 1:
            clr1 = aquaBlue.getLerped(fullWhite, ofMap(sin(ofGetElapsedTimef()+500),-1.,1.,0.,1.));
            clr2= blancCasse.getLerped(skinColor, ofMap(sin(ofGetElapsedTimef()*0.7),-1.,1.,0.,1.));
            clr3=aquaBlue.getLerped(fullWhite, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            break;
            
        case 2:
            clr1 = heetchRed.getLerped(fullWhite, ofMap(sin(ofGetElapsedTimef()+500),-1.,1.,0.,1.));
            clr2= blancCasse.getLerped(fullWhite, ofMap(sin(ofGetElapsedTimef()*0.7),-1.,1.,0.,1.));
            clr3=aquaBlue.getLerped(fullWhite, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            break;
            
        case 3:
            
            clr1 = blancCasse.getLerped(fullWhite, ofMap(sin(ofGetElapsedTimef()+500),-1.,1.,0.,1.));
            clr2= heetchRed.getLerped(heetchRed, ofMap(sin(ofGetElapsedTimef()*0.7),-1.,1.,0.,1.));
            clr3=fullWhite.getLerped(blancCasse, ofMap(sin(ofGetElapsedTimef()-200),-1.,1.,0.,1.));
            break;
            
        case 4:
            clr1 = blancCasse.getLerped(fullWhite, ofMap(sin(ofGetElapsedTimef()+500),-1.,1.,0.,1.));
            clr2= heetchRed.getLerped(heetchRed, ofMap(sin(ofGetElapsedTimef()*0.7),-1.,1.,0.,1.));
            clr3=fullWhite.getLerped(lightBlack, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            break;
            
        case 5:
            clr1 = pastelGreen.getLerped(blancCasse, ofMap(sin(ofGetElapsedTimef()+500),-1.,1.,0.,1.));
            clr2= fullWhite.getLerped(darkPastelGreen, ofMap(sin(ofGetElapsedTimef()*0.7),-1.,1.,0.,1.));
            clr3=fullWhite.getLerped(lightBlack, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            break;
            
        case 6:
            clr1 = pastelGreen.getLerped(neonGreen, ofMap(sin(ofGetElapsedTimef()+500),-1.,1.,0.,1.));
            clr2= fullWhite.getLerped(darkPastelGreen, ofMap(sin(ofGetElapsedTimef()*0.7),-1.,1.,0.,1.));
            clr3=fullWhite.getLerped(lightBlack, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            break;
            
        case 7:
            clr1 = neonRed.getLerped(offNeonRed, ofMap(sin(ofGetElapsedTimef()+500),-1.,1.,0.,1.));
            clr2= fullWhite.getLerped(offNeonRed, ofMap(sin(ofGetElapsedTimef()*0.7),-1.,1.,0.,1.));
            clr3=fullWhite.getLerped(darkPastelGreen, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            break;

        case 8:
            clr1 = neonGreen.getLerped(neonRed, ofMap(sin(ofGetElapsedTimef()+500),-1.,1.,0.,1.));
            clr2= fullWhite.getLerped(offNeonRed, ofMap(sin(ofGetElapsedTimef()*0.7),-1.,1.,0.,1.));
            clr3=fullWhite.getLerped(lightBlack, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            break;

        default:
            break;
    }
    ofEnableAlphaBlending();
    
    clr1.a = alpha;
    clr2.a = alpha;
    clr3.a = alpha;
    
    ofSetColor(clr1);
    circle_sin.close(); // to connect first and last point of our shape we need to use ‘close’ function
    circle_sin= circle_sin.getSmoothed(smoothing_amount);
    circle_sin.draw();
    
    ofSetColor(clr2);
    circle_cos.close();
    circle_cos= circle_cos.getSmoothed(smoothing_amount);
    circle_cos.draw();
    
    ofSetColor(clr3);
    circle_un_cos.close();
    circle_un_cos= circle_un_cos.getSmoothed(smoothing_amount);
    circle_un_cos.draw();
    
    ofDisableAlphaBlending();
    ofPopMatrix();
    ofPopStyle();
   
}
