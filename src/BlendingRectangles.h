//
//  BlendingRectangles.hpp
//  mySketch
//
//  Created by Michael Sedbon on 28/11/2016.
//
//

#ifndef BlendingRectangles_h
#define BlendingRectangles_h

#include "ofMain.h"
#include "ofxSyphon.h"
#include "ofxFft.h"

#define MAX_RECT = 20

class BlendingRectangles {
public:
    void setup();
    void draw();
    
    ofParameterGroup Parameters;
    ofParameter<int> rectWidth, rectHeight;
    ofParameter<int> nbr;
    
    ofxSyphonServer OutputSyphonServer;
    
    bool created;
    
    float movement;
    
    bool direction;
    
    //ofPoint rects[MAX_RECT];
    //int directions[MAX_RECT];
    
    float lowAvg;
};

#endif /* BlendingRectangles_hpp */
