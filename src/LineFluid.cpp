//
//  LineFluid.cpp
//  mySketch
//
//  Created by Michael Sedbon on 28/11/2016.
//
//

#include "LineFluid.h"


void LineFluid::setup(){

    Parameters.setName("LineFluid");
    
    
    OutputSyphonServer.setName("LineFluid Server");
    Parameters.add(lowPct.set("lowPct", 0.2, 0.0, 1.0));
    Parameters.add(highPct.set("highPct", 0.8, 0.0, 1.0));
    Parameters.add(speedCoef.set("speedCoef", 1., 0.0, 10.));
    Parameters.add(height.set("height", 100, 0, 400));
    Parameters.add(waveNumber.set("waveNumber", 2, 0, 15));
    Parameters.add(normals.set("normals", false));
    Parameters.add(nbNormals.set("Nb Normals", 5000, 0, 10000));
    Parameters.add(normalLength.set("normalLength", 50, 0, 300));
    
    nbColorBands = 3;
    palette.resize(3);
    setColors.resize(nbColorBands);
    storedNbColorBands = nbColorBands;
    colorMode = 0;
    storedColorMode = colorMode;
    alpha = 255;
    storedAlpha = alpha;
    lineWidth = 5;
}

void LineFluid::draw(){
    
    ofPushStyle();
    ofPushMatrix();
    ofEnableAlphaBlending();
    
    ofTranslate(0,(ofGetHeight()/2) - (lineWidth/2)); // move initital drawing postion to the center of the screen
    
    if (storedAlpha != alpha) {
        storedAlpha = alpha;
        for (int i = 0; i < setColors.size(); i++) {
            setColors[i].a = alpha;
        }
        fullWhite.a = alpha;
    }
    
    if (storedNbColorBands != nbColorBands or storedColorMode != colorMode) {
        setColors.resize(nbColorBands);
        storedNbColorBands = nbColorBands;
        
        for (int n = 0; n < setColors.size(); n++) {
            setColors[n] = palette[(int) ofRandom(0, palette.size())];
            setColors[n].a = alpha;
        }
        storedColorMode = colorMode;
    }

    
    vector<ofPolyline> lines;
    lines.resize(nbColorBands);
    
    int smoothing_amount=50;
    int anim_shape=waveNumber;
    
    float speed_increment=ofGetElapsedTimef()*speedCoef;
    
    ofSetColor(255);// set color drawing color to white
    
    if (highPct < lowPct) {
        for (int k = 0; k < lines.size(); k++) {
            for (int i = 0; i < ofGetWidth()/nbColorBands + 40; i++) {
                lines[k].addVertex(ofPoint(i + k*ofGetWidth()/nbColorBands, 0));
            }
        }
    }
    else {
        for (int k = 0; k < lines.size(); k++) {
            for (int i = 0; i < ofGetWidth()/nbColorBands + 40; i++) {
                if (i + k*ofGetWidth()/nbColorBands < ofGetWidth()*lowPct or i + k*ofGetWidth()/nbColorBands >= ofGetWidth()*highPct) {
                    lines[k].addVertex(ofPoint(i + k*ofGetWidth()/nbColorBands, 0));
                }
                else {
                    float angle=TWO_PI/ofGetWidth()*(i + k*ofGetWidth()/nbColorBands);
                    float y = sin((angle+speed_increment)*waveNumber)*height;
                    
                    lines[k].addVertex(ofPoint(i + k*ofGetWidth()/nbColorBands, y));
                }
            }
        }
    }
    
    ofSetLineWidth(lineWidth);
    
    for (int i = 0; i < lines.size(); i++) {
        lines[i] = lines[i].getSmoothed(smoothing_amount);
        ofSetColor(fullWhite.getLerped(setColors[i], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
        lines[i].draw();
        if (normals) {
            float realNbNormals = nbNormals / nbColorBands;
            ofSetLineWidth(1);
            for (int p=0; p<realNbNormals; p+=1) {
                ofVec3f point = lines[i].getPointAtPercent((float) p/realNbNormals);
                float floatIndex = lines[i].getIndexAtPercent((float) p/realNbNormals);
                ofVec3f normal = lines[i].getNormalAtIndexInterpolated(floatIndex) * normalLength;
                ofDrawLine(point-normal/2, point+normal/2);
            }
        }
    }
    
    ofDisableAlphaBlending();
    ofPopStyle();
    ofPopMatrix();
}

