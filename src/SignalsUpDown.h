//
//  SignalsUpDown.h
//  mySketch 27_11_V2 2
//
//  Created by Taf on 30/11/2016.
//
//

#ifndef __mySketch_27_11_V2_2__SignalsUpDown__
#define __mySketch_27_11_V2_2__SignalsUpDown__

#include "ofMain.h"
#include "ofxSyphon.h"
#include "ofxFft.h"

#define MAX_PTS 500
#define FCTN_PTS 10

class SignalsUpDown {
public:
    void setup();
    void draw();
    
    ofParameterGroup Parameters;
    ofParameter<int> baseSpeed;
    ofParameter<float> coeffSpeed;
    int mode, nbr, height;
    
    ofxSyphonServer OutputSyphonServer;
    
    int bufferSize = 256;
    
    ofPoint pts[MAX_PTS];
    int nPts;
    int storedMode, storedNbr;
    bool created;
    
    int it;
    
    int dsc;
    bool move;
    
    float lowAvg, threshold;
    
    int nbColorBands, storedNbColorBands;
    vector<ofColor> palette, setColors;
    ofColor fullWhite = ofColor::fromHex(0xFFFFFF);
    int colorMode, storedColorMode, alpha, storedAlpha, lineWidth;
};

#endif /* defined(__mySketch_27_11_V2_2__SignalsUpDown__) */
