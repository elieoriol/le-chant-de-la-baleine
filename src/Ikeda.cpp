//
//  Ikeda.cpp
//  Le Chant de la Baleine
//
//  Created by Michael Sedbon on 20/03/2017.
//
//

#include "Ikeda.h"


void Ikeda::setup(){
    
    w = ofGetWidth();
    h = ofGetHeight();
    
    soundValue = 25;
    
    previousIsWhite = false;
    
    tileCountY.resize(80);
    for (int i = 0 ; i < tileCountY.size(); i++) {
        tileCountY[i] = ofRandom(20,40);
    }
    
    
    
    
}

//-----------------------


void Ikeda::update(){
    
    
}


//--------------------------

void Ikeda::draw(){
    
    
    tileCountX = (int) ofMap(ofGetMouseX(),0,w,2,100);
    for (int i = 0; i< soundValue; i++) {
        tileCountY[i] = w / (soundValue + ofNoise(soundValue));
    }
    
    for (int gridY = 0; gridY < tileCountY[gridY]; gridY ++) {
        for (int gridX; gridX< tileCountX; gridX++) {
            tileWidth = ofGetWidth() / tileCountX;
            tileHeight = ofGetHeight() / tileCountY[gridY];
            posX = tileWidth * gridX;
            posY = tileHeight * gridY;
            
        }
    }

    
    
    if (previousIsWhite) {
        ofSetColor(0);
        previousIsWhite = !previousIsWhite;
    } else {
        ofSetColor(255);
        previousIsWhite = !previousIsWhite;
    }

    ofDrawRectangle(posX, posY, tileWidth, tileHeight);
    
    
    
    
    
    
}