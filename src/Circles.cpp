//
//  Circles.cpp
//  mySketch
//
//  Created by Taf on 24/11/2016.
//
//

#include "Circles.h"

void Circles::setup() {
    Parameters.setName("Concentric Circles");
    
    Parameters.add(nbMaxCircles.set("nbMaxCircles", 20, 0, 50));
    Parameters.add(initRadius.set("initRadius", 100, 0, 500));
    Parameters.add(finalRadius.set("finalRadius", 300, 0, ofGetHeight()));
    Parameters.add(fadeDistance.set("fadeDistance", 100, 0, ofGetHeight()));
    Parameters.add(radialSpeed.set("radialSpeed", 1, 0, 10));
    
    
    OutputSyphonServer.setName("Concentric Circles Server");
    
    lowAvg = 0;
    threshold = 0.5;
    
    nbColorBands = 3;
    palette.resize(3);
    setColors.resize(nbColorBands);
    storedNbColorBands = nbColorBands;
    colorMode = 0;
    storedColorMode = colorMode;
    alpha = 255;
    storedAlpha = alpha;
    lineWidth = 5;
}



void Circles::draw() {
    ofPushMatrix();
    ofPushStyle();
    ofEnableAlphaBlending();
    
    ofTranslate(ofGetWidth()/2, ofGetHeight()/2, 0);
    
    palette.push_back(fullWhite);
    
    if (storedAlpha != alpha) {
        storedAlpha = alpha;
        for (int i = 0; i < setColors.size(); i++) {
            setColors[i].a = alpha;
        }
    }
    
    if (storedColorMode != colorMode) {
        for (int n = 0; n < setColors.size(); n++) {
            setColors[n] = palette[(int) ofRandom(0, palette.size())];
            setColors[n].a = alpha;
        }
        storedColorMode = colorMode;
    }
    
    if (lowAvg > threshold and nbCircles < nbMaxCircles and !above) {
        above = true;
        setColors[nbCircles] = ofColor::fromHex(palette[(int) ofRandom(0, palette.size())].getHex());
        radiuses[nbCircles++] = initRadius;
    }
    if (lowAvg < threshold) above = false;
    
    for (int i = 0; i < nbCircles; i++) {
        radiuses[i] += radialSpeed;
        
        if (radiuses[i] > finalRadius) {
            for (int j = i + 1; j < nbCircles; j++) {
                radiuses[j-1] = radiuses[j];
                setColors[j-1] = setColors[j];
            }
            nbCircles--;
        }
        else if (finalRadius - radiuses[i] < fadeDistance) {
            setColors[i].a = alpha*(finalRadius - radiuses[i])/fadeDistance;
        }
        ofSetColor(setColors[i]);
        ofNoFill();
        ofDrawCircle(0, 0, radiuses[i]);
    }
    
    ofSetColor(255);
    
    ofDisableAlphaBlending();
    ofPopMatrix();
    ofPopStyle();
}
