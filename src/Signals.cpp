//
//  Signal.cpp
//  mySketch 27_11_V2 2
//
//  Created by Taf on 30/11/2016.
//
//

#include "Signals.h"

void Signals::setup() {
    Parameters.setName("Signals");
    
    Parameters.add(baseSpeed.set("baseSpeed", 3, 0, 20));
    Parameters.add(coeffSpeed.set("CoeffSpeed", 5., 0., 10.));
    Parameters.add(constantMode.set("constantMode", false));
    Parameters.add(constantSpeed.set("constantSpeed", 5, 0, 10));
    
    OutputSyphonServer.setName("Signals Server");
    
    
    nPts = 0;
    created = false;
    
    lowAvg = 0.;
    threshold = 0.;
    
    nbColorBands = 3;
    palette.resize(3);
    setColors.resize(nbColorBands);
    storedNbColorBands = nbColorBands;
    colorMode = 0;
    storedColorMode = colorMode;
    alpha = 255;
    storedAlpha = alpha;
    lineWidth = 5;
}



void Signals::draw() {
    ofTranslate(0, ofGetHeight()/2, 0);
    ofEnableAlphaBlending();
    
    if (storedAlpha != alpha) {
        storedAlpha = alpha;
        for (int i = 0; i < setColors.size(); i++) {
            setColors[i].a = alpha;
        }
        fullWhite.a = alpha;
    }
    
    if (storedNbColorBands != nbColorBands or storedColorMode != colorMode) {
        setColors.resize(nbColorBands);
        storedNbColorBands = nbColorBands;
        
        for (int n = 0; n < setColors.size(); n++) {
            setColors[n] = palette[(int) ofRandom(0, palette.size())];
            setColors[n].a = alpha;
        }
        storedColorMode = colorMode;
    }
    
    switch (mode) {
        case 1:
            nPts = nbr*4;
            break;
        
        default:
            nPts = nbr*2;
            break;
    }
    
    if(storedMode != mode or storedNbr != nbr) {
        storedMode = mode;
        storedNbr = nbr;
        created = false;
    }
    
    if (!created) {
        switch (mode) {
            case 1:
                for (int i = 0; i < nPts; i+=4) {
                    pts[i].x = i*256/nbr;
                    pts[i].y = -height/2;
                    pts[i+1].x = (i+2)*256/nbr;
                    pts[i+1].y = -height/2;
                    pts[i+2].x = (i+2)*256/nbr;
                    pts[i+2].y = height/2;
                    pts[i+3].x = (i+4)*256/nbr;
                    pts[i+3].y = height/2;
                }
                break;
                
            case 2:
                for (int i = 0; i < nPts; i+=2) {
                    pts[i].x = i*512/nbr;
                    pts[i].y = height/2;
                    pts[i+1].x = (i+1)*512/nbr;
                    pts[i+1].y = -height/2;
                }
                break;
                
            case 3:
                for (int i = 0; i < nPts; i+=2) {
                    pts[i].x = i*512/nbr;
                    pts[i].y = height/2;
                    pts[i+1].x = (i+2)*512/nbr;
                    pts[i+1].y = -height/2;
                }
                break;
        }
        
        for (int i = nPts; i < MAX_PTS; i++) {
            pts[i].x = 0;
            pts[i].y = 0;
        }
        created = true;
    }
    
    for (int i = nPts; i >= 0; i--) {
        pts[i].x += baseSpeed;
        if (lowAvg > threshold) {
            if (constantMode) pts[i].x += constantSpeed;
            else pts[i].x += ofClamp((lowAvg-threshold)*coeffSpeed, 1, 10);
        }
        if (pts[i].x >= 1024) pts[i].x -= 1024;
        if (pts[i].y <= 0) pts[i].y = -height/2;
        else pts[i].y = height/2;
    }
    
    int rangeMin = 0;
    
    for (int i = 0; i < nPts-1; i++) {
        if (pts[i].x > pts[i+1].x) {
            rangeMin = i+1;
            break;
        }
    }
    
    
    ofSetLineWidth(lineWidth);
    int k = 0, c = 0;
    
    for (int i = rangeMin; i < nPts-1; i++) {
        if (c++ == (int) nPts/nbColorBands - 1) {
            ofSetColor(fullWhite.getLerped(setColors[k++], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
            c = 0;
        }
        else ofSetColor(fullWhite.getLerped(setColors[k], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
        ofDrawLine(pts[i].x, pts[i].y, pts[i+1].x, pts[i+1].y);
    }
    for (int i = 0; i < rangeMin-1; i++) {
        if (c++ == (int) nPts/nbColorBands - 1) {
            ofSetColor(fullWhite.getLerped(setColors[k++], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
            c = 0;
        }
        else ofSetColor(fullWhite.getLerped(setColors[k], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
        ofDrawLine(pts[i].x, pts[i].y, pts[i+1].x, pts[i+1].y);
    }
    if (rangeMin != 0) {
        if (c++ == (int) nPts/nbColorBands - 1) {
            ofSetColor(fullWhite.getLerped(setColors[k++], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
            c = 0;
        }
        else ofSetColor(fullWhite.getLerped(setColors[k], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
        ofDrawLine(pts[nPts-1].x, pts[nPts-1].y, pts[0].x, pts[0].y);
    }
    switch (mode) {
        case 1:
            if (rangeMin != 0) {
                if (c++ == (int) nPts/nbColorBands - 1) {
                    ofSetColor(fullWhite.getLerped(setColors[k++], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                    c = 0;
                }
                else ofSetColor(fullWhite.getLerped(setColors[k], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                ofDrawLine(0, pts[rangeMin].y, pts[rangeMin].x, pts[rangeMin].y);
                
                if (c++ == (int) nPts/nbColorBands - 1) {
                    ofSetColor(fullWhite.getLerped(setColors[k++], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                    c = 0;
                }
                else ofSetColor(fullWhite.getLerped(setColors[k], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                ofDrawLine(pts[rangeMin-1].x, pts[rangeMin-1].y, 1024, pts[rangeMin-1].y);
            }
            break;
                
        case 2:
            if (rangeMin!=0) {
                if (pts[rangeMin].y >= 0) {
                    if (c++ == (int) nPts/nbColorBands - 1) {
                        ofSetColor(fullWhite.getLerped(setColors[k++], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                        c = 0;
                    }
                    else ofSetColor(fullWhite.getLerped(setColors[k], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                    ofDrawLine(0, height*(0.5 - pts[rangeMin].x*nbr/512), pts[rangeMin].x, pts[rangeMin].y);
                    
                    if (c++ == (int) nPts/nbColorBands - 1) {
                        ofSetColor(fullWhite.getLerped(setColors[k++], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                        c = 0;
                    }
                    else ofSetColor(fullWhite.getLerped(setColors[k], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                    ofDrawLine(pts[rangeMin-1].x, pts[rangeMin-1].y, 1024, height*(0.5 - pts[rangeMin].x*nbr/512));
                } else {
                    if (c++ == (int) nPts/nbColorBands - 1) {
                        ofSetColor(fullWhite.getLerped(setColors[k++], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                        c = 0;
                    }
                    else ofSetColor(fullWhite.getLerped(setColors[k], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                    ofDrawLine(0, -height*(0.5 - pts[rangeMin].x*nbr/512), pts[rangeMin].x, pts[rangeMin].y);
                    
                    if (c++ == (int) nPts/nbColorBands - 1) {
                        ofSetColor(fullWhite.getLerped(setColors[k++], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                        c = 0;
                    }
                    else ofSetColor(fullWhite.getLerped(setColors[k], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                    ofDrawLine(pts[rangeMin-1].x, pts[rangeMin-1].y, 1024, -height*(0.5 - pts[rangeMin].x*nbr/512));
                }
            } else {
                if (c++ == (int) nPts/nbColorBands - 1) {
                    ofSetColor(fullWhite.getLerped(setColors[k++], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                    c = 0;
                }
                else ofSetColor(fullWhite.getLerped(setColors[k], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                ofDrawLine(0, height*(0.5 - pts[rangeMin].x*nbr/512), pts[rangeMin].x, pts[rangeMin].y);
                
                if (c++ == (int) nPts/nbColorBands - 1) {
                    ofSetColor(fullWhite.getLerped(setColors[k++], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                    c = 0;
                }
                else ofSetColor(fullWhite.getLerped(setColors[k], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                ofDrawLine(pts[nPts-1].x, pts[nPts-1].y, 1024, height*(0.5 - pts[nPts].x*nbr/512));
            }
            break;
                
        case 3:
            if (pts[rangeMin].y >= 0) {
                if (c++ == (int) nPts/nbColorBands - 1) {
                    ofSetColor(fullWhite.getLerped(setColors[k++], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                    c = 0;
                }
                else ofSetColor(fullWhite.getLerped(setColors[k], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                ofDrawLine(0, height*(0.5 - pts[rangeMin].x*nbr/1024), pts[rangeMin].x, pts[rangeMin].y);
                
                if (c++ == (int) nPts/nbColorBands - 1) {
                    ofSetColor(fullWhite.getLerped(setColors[k++], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                    c = 0;
                }
                else ofSetColor(fullWhite.getLerped(setColors[k], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                ofDrawLine(pts[rangeMin-1].x, pts[rangeMin-1].y, 1024, height*(0.5 - pts[rangeMin].x*nbr/1024));
            } else {
                if (c++ == (int) nPts/nbColorBands - 1) {
                    ofSetColor(fullWhite.getLerped(setColors[k++], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                    c = 0;
                }
                else ofSetColor(fullWhite.getLerped(setColors[k], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                ofDrawLine(0, -height*(0.5 - pts[rangeMin].x*nbr/1024), pts[rangeMin].x, pts[rangeMin].y);
                
                if (c++ == (int) nPts/nbColorBands - 1) {
                    ofSetColor(fullWhite.getLerped(setColors[k++], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                    c = 0;
                }
                else ofSetColor(fullWhite.getLerped(setColors[k], ofMap(sin(ofGetElapsedTimef()), -1., 1., 0., 1.)));
                ofDrawLine(pts[rangeMin-1].x, pts[rangeMin-1].y, 1024, -height*(0.5 - pts[rangeMin].x*nbr/1024));
            }
            break;
    }
    
    
    ofDisableAlphaBlending();
    
    ofTranslate(0, -ofGetHeight()/2, 0);
}

