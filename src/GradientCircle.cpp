//
//  GradientCircle.cpp
//  mySketch
//
//  Created by Michael Sedbon on 27/11/2016.
//
//

#include "GradientCircle.h"

void GradientCircle::setup(){
    
    
    
    OutputSyphonServer.setName("Gradient");

    
    color = 1;
    time = ofGetElapsedTimeMillis();
    
}

void GradientCircle::draw(){
    
    
    clr1 = freshYellow.getLerped(orangeFire, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
    clr2 = orangeFire.getLerped(freshYellow, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
    
    switch (color) {
        case 1:
            clr1 = freshYellow.getLerped(orangeFire, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            clr2 = orangeFire.getLerped(freshYellow, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            break;
        case 2 :
            
            clr1 = orangeFire.getLerped(freshYellow, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            clr2 = bleuVert.getLerped(vertDollar, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            break;
            
            
        case 3 :
            
            clr1 = bleuVert.getLerped(vertDollar, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            clr2 = vertDollar.getLerped(blancCasse, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            break;
            
            
        case 4 :
            
            clr1 = vertDollar.getLerped(blancCasse, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            clr2 = blancCasse.getLerped(pinotBleu, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            break;
            
        case 5 :
            
            clr1 = blancCasse.getLerped(pinotBleu, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            clr2 = pinotBleu.getLerped(magentaLove, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            break;
            
        case 6 :
            
            clr1 = pinotBleu.getLerped(magentaLove, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            clr2 = magentaLove.getLerped(greenLake, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            break;
            
        case 7 :
            
            clr1 = magentaLove.getLerped(greenLake, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            clr2 = greenLake.getLerped(freshYellow, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            break;
            
        case 8 :
            
            clr1 = greenLake.getLerped(freshYellow, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            clr2 = freshYellow.getLerped(orangeFire, ofMap(sin(ofGetElapsedTimef()),-1.,1.,0.,1.));
            break;
            
            
    }
    
    if (sin(ofGetElapsedTimef()) > 0.99 and ofGetElapsedTimeMillis() - time > 100) {
        if (color < 8) color++;
        else color = 1;
        time = ofGetElapsedTimeMillis();
    }
    
        ofBackgroundGradient(clr1, clr2, OF_GRADIENT_LINEAR);
    
   /* ofPushMatrix();
    ofRotate(90, ofGetWidth()/2, ofGetHeight()/2, 0);

    ofPopMatrix();*/
    
}

