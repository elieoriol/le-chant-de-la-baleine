//
//  Ikeda.hpp
//  Le Chant de la Baleine
//
//  Created by Michael Sedbon on 20/03/2017.
//
//

#ifndef Ikeda_h
#define Ikeda_h

#include "Ikeda.h"
#include "ofMain.h"

#include "ofxSyphon.h"
#include "ofxFft.h"

class Ikeda {
public:
    void setup();
    void draw();
    void update();
    
    ofxSyphonServer OutputSyphonServer;
    
    
    int lowAvg, threshold;

    int tileCountX = 3;
    vector <int> tileCountY;
    
    int soundValue;
    
    int w,h;
    
    float tileWidth, tileHeight;
    float posX, posY;
    
    bool previousIsWhite;
};

#endif /* Ikeda_hpp */
