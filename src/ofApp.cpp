#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    
    ofSetEscapeQuitsApp(false);
    
    ofSetFrameRate(60);
    
    ofSetVerticalSync(true);
    ofSetCircleResolution(80);
    ofBackground(0, 0, 0,0);
    
    soundStream.printDeviceList();
    
    //if you want to set a different device id
    soundStream.setDeviceID(2);
    //bear in mind the device id corresponds to all audio devices, including  input-only and output-only devices
    
    // 0 output channels,
    // 2 input channels
    // 44100 samples per second
    // 256 samples per buffer
    // 4 num buffers (latency)
    soundStream.setup(this, 0, 2, 44100, bufferSize, 4);
    
    fft = ofxFft::create(bufferSize, OF_FFT_WINDOW_HAMMING);
    audioBins.resize(fft->getBinSize());
    
    
    //--------------Syphon-----------------
    mClient.setup();
    
    //using Syphon app Simple Server, found at http://syphon.v002.info/
    mClient.set("","Simple Server");
    
    
    //--------------Shapes-----------------
    
    waveForm.setup();
    signal.setup();
    upDown.setup();
    signalUpDown.setup();
    fluid.setup();
    gradientCircle.setup();
    movingCircle.setup();
    lineFluid.setup();
    circles.setup();
    ikeda.setup();
    
    
    //------------Palettes----------------
    palettes[0].resize(1);
    palettes[1].resize(2);
    palettes[2].resize(3);
    palettes[3].resize(3);
    palettes[4].resize(3);
    palettes[5].resize(3);
    palettes[6].resize(3);
    palettes[7].resize(3);
    palettes[8].resize(3);
    palettes[9].resize(3);
    
    palettes[0][0] = fullWhite;
    
    palettes[1][0] = blancCasse;
    palettes[1][1] = lightBlack;
    
    palettes[2][0] = pastelBlue1;
    palettes[2][1] = pastelBlue2;
    palettes[2][2] = pastelBlue3;
    
    palettes[3][0] = aquaBlue;
    palettes[3][1] = heetchRed;
    palettes[3][2] = skinColor;
    
    palettes[4][0] = denseBlue;
    palettes[4][1] = blancCasse;
    palettes[4][2] = lightBlack;
    
    palettes[5][0] = sadBlue;
    palettes[5][1] = deepBlue;
    palettes[5][2] = blancCasse;
    
    palettes[6][0] = heetchRed;
    palettes[6][1] = blancCasse;
    palettes[6][2] = lightBlack;
    
    palettes[7][0] = skinColor;
    palettes[7][1] = rossbeefSkin;
    palettes[7][2] = heetchRed;
    
    palettes[8][0] = pastelGreen;
    palettes[8][1] = darkPastelGreen;
    palettes[8][2] = darkPastelRed;
    
    palettes[9][0] = neonGreen;
    palettes[9][1] = neonRed;
    palettes[9][2] = offNeonRed;
    
    
    //--------------GUI---------------------
    master.setName("Master");
    master.add(gradientBackground.set("Gradient Background", false));
    
    fftMaster.setName("FFT");
    fftMaster.add(bandDivide.set("bandDivide", 15, 8, 30));
    fftMaster.add(fftBand.set("fftBand", 2, 0, 7));
    fftMaster.add(threshold.set("threshold", 0.15, 0., 1.));
    
    aspectMaster.setName("Aspect");
    aspectMaster.add(alpha.set("alpha", 255, 0, 255));
    aspectMaster.add(lineWidth.set("lineWidth", 5, 1, 10));
    aspectMaster.add(colorMode.set("colorMode", 0, 0, 9));
    aspectMaster.add(nbColorBands.set("nbColorBands", 3, 1, 5));
    
    signalMaster.setName("Signal");
    signalMaster.add(mode.set("mode", 1, 1, 3));
    signalMaster.add(nbr.set("nbr", 4, 1, 30));
    signalMaster.add(height.set("height", 128, 1, 512));
    
    master.add(fftMaster);
    master.add(aspectMaster);
    master.add(signalMaster);
    
    masterGui.setup(master);
    waveFormGui.setup(waveForm.Parameters);
    signalGui.setup(signal.Parameters);
    signalUpDownGui.setup(signalUpDown.Parameters);
    fluidGui.setup(fluid.Parameters);
    movingCircleGui.setup(movingCircle.Parameters);
    lineFluidGui.setup(lineFluid.Parameters);
    circlesGui.setup(circles.Parameters);
    
    masterGui.setHeaderBackgroundColor(pastelBlue1);
    waveFormGui.setHeaderBackgroundColor(rossbeefSkin);
    signalGui.setHeaderBackgroundColor(denseBlue);
    signalUpDownGui.setHeaderBackgroundColor(pastelGreen);
    fluidGui.setHeaderBackgroundColor(neonGreen);
    movingCircleGui.setHeaderBackgroundColor(darkPastelRed);
    lineFluidGui.setHeaderBackgroundColor(offNeonRed);
    
    allParameters.add(master);
    allParameters.add(waveForm.Parameters);
    allParameters.add(signal.Parameters);
    allParameters.add(signalUpDown.Parameters);
    allParameters.add(fluid.Parameters);
    allParameters.add(movingCircle.Parameters);
    allParameters.add(lineFluid.Parameters);
    allParameters.add(circles.Parameters);
    
    hideGUI = false;
    lock = true;
    //soundDeviceList = false;
    //midiDeviceList = false;
    //setMidi1 = false;
    //setMidi2 = false;
    
    inputDevice.setName("Devices");
    inputDevice.add(soundDeviceId.set("Sound Device ID", 2, 0, 10));
    inputDevice.add(midiDevice1Id.set("Midi Device 1 ID", 1, 0, 10));
    inputDevice.add(midiDevice2Id.set("Midi Device 2 ID", 2, 0, 10));
    inputDeviceGui.setup(inputDevice);
    
    storedSoundDeviceId = soundDeviceId;
    storedMidiDevice1Id = midiDevice1Id;
    storedMidiDevice2Id = midiDevice2Id;
    
    
    //--------------MIDI-------------------
    //sync1.setup(midiDevice1Id, allParameters);
    //sync2.setup(midiDevice2Id, allParameters);
}


//--------------------------------------------------------------
void ofApp::draw(){
    
    //COLORS
    
    waveForm.nbColorBands = nbColorBands;
    waveForm.alpha = alpha;
    waveForm.palette = palettes[colorMode];
    waveForm.colorMode = colorMode;
    signal.nbColorBands = nbColorBands;
    signal.alpha = alpha;
    signal.palette = palettes[colorMode];
    signal.colorMode = colorMode;
    upDown.nbColorBands = nbColorBands;
    upDown.alpha = alpha;
    upDown.palette = palettes[colorMode];
    upDown.colorMode = colorMode;
    signalUpDown.nbColorBands = nbColorBands;
    signalUpDown.alpha = alpha;
    signalUpDown.palette = palettes[colorMode];
    signalUpDown.colorMode = colorMode;
    lineFluid.nbColorBands = nbColorBands;
    lineFluid.alpha = alpha;
    lineFluid.palette = palettes[colorMode];
    lineFluid.colorMode = colorMode;
    circles.nbColorBands = nbColorBands;
    circles.alpha = alpha;
    circles.palette = palettes[colorMode];
    circles.colorMode = colorMode;
    movingCircle.alpha = alpha;
    fluid.alpha = alpha;
    
    //FFT
    
    float lowAvg = 0.;
    int bds = (int) fft->getBinSize()/bandDivide;
    
    for (int i = 0; i < bds; i++) {
        lowAvg += abs(audioBins[i + bds*fftBand]);
    }
    lowAvg /= bds;
    
    signal.lowAvg = lowAvg;
    signalUpDown.lowAvg = lowAvg;
    upDown.lowAvg = lowAvg;
    movingCircle.lowAvg = lowAvg;
    circles.lowAvg = lowAvg;
    ikeda.lowAvg = lowAvg;
    signal.threshold = threshold;
    signalUpDown.threshold = threshold;
    upDown.threshold = threshold;
    movingCircle.threshold = threshold;
    circles.threshold = threshold;
    ikeda.threshold = threshold;
    
    
    //SIGNALS
    
    signal.mode = mode;
    signalUpDown.mode = mode;
    upDown.mode = mode;
    signal.nbr = nbr;
    signalUpDown.nbr = nbr;
    upDown.nbr = nbr;
    signal.height = height;
    signalUpDown.height = height;
    upDown.height = height;
    
    
    //LINEWIDTH
    
    fluid.lineWidth = lineWidth;
    lineFluid.lineWidth = lineWidth;
    movingCircle.lineWidth = lineWidth;
    waveForm.lineWidth = lineWidth;
    signal.lineWidth = lineWidth;
    upDown.lineWidth = lineWidth;
    signalUpDown.lineWidth = lineWidth;
    circles.lineWidth = lineWidth;
    
    
    ofBackground(0);
    gradientCircle.draw();
    ofFill();
    mClient.draw(0,0);
    gradientCircle.OutputSyphonServer.publishScreen();
    
    ofBackground(0);
    waveForm.draw();
    ofFill();
    mClient.draw(0,0);
    waveForm.OutputSyphonServer.publishScreen();
    
    ofBackground(0);
    signal.draw();
    ofFill();
    mClient.draw(0,0);
    signal.OutputSyphonServer.publishScreen();
    
    ofBackground(0);
    upDown.draw();
    ofFill();
    mClient.draw(0,0);
    upDown.OutputSyphonServer.publishScreen();
    
    ofBackground(0);
    signalUpDown.draw();
    ofFill();
    mClient.draw(0,0);
    signalUpDown.OutputSyphonServer.publishScreen();
    
    ofBackground(0);
    fluid.draw();
    ofFill();
    mClient.draw(0,0);
    fluid.OutputSyphonServer.publishScreen();
    
    ofBackground(0);
    movingCircle.draw();
    ofFill();
    mClient.draw(0,0);
    movingCircle.OutputSyphonServer.publishScreen();
    
    ofBackground(0);
    lineFluid.draw();
    ofFill();
    mClient.draw(0,0);
    lineFluid.OutputSyphonServer.publishScreen();
    
    ofBackground(0);
    circles.draw();
    ofFill();
    mClient.draw(0,0);
    circles.OutputSyphonServer.publishScreen();
    
    ofBackground(0);
    
    
    if (gradientBackground) {
        gradientCircle.draw();
    }
    
    switch (caseKey) {
        case 0:
            //----------------- Wave Form ------------------
            waveForm.draw();
            break;
            
            
        case 1:
            //----------------- Creneaux ------------------
            signal.draw();
            break;
            
        case 2:
            //----------------- Up & Down ------------------
            upDown.draw();
            break;
            
        case 3:
            //----------------- Fluid ------------------
            fluid.draw();
            break;
            
        case 4:
            //----------------- Circle ------------------
            movingCircle.draw();
            break;
            
            
        case 5:
            //----------------- Line Fluid ------------------
            lineFluid.draw();
            break;
            
        case 6:
            //----------------- Signal Up & Down ------------
            signalUpDown.draw();
            break;
        
        case 7:
            //----------------- Concentric Circles ------------
            circles.draw();
            break;
            
        case 8:
            //---------------------Ikeda---------------
            ikeda.draw();
            break;
    }
    
    
    //--------------GUI---------------------
    if (lock) {
        ofPushStyle();
        ofFill();
        ofSetColor(neonRed);
        ofDrawCircle(950,80,12);
        ofDrawBitmapString("IT IS LOCKED", 900, 110);
        ofPopStyle();
    }
    else {
        ofPushStyle();
        ofFill();
        ofSetColor(pastelGreen);
        ofDrawCircle(950,80,12);
        ofPopStyle();
    }
    ofSetColor(255);
    ofDrawBitmapString("press h to hide/unhide GUI", 700, 20);
    
    if (!hideGUI) {
        ofDrawBitmapString("press y to Concentric Circles", 700, 40);
        ofDrawBitmapString("press u to WaveForm", 700, 60);
        ofDrawBitmapString("press i to Signal", 700, 80);
        ofDrawBitmapString("press o to Up & Down", 700, 100);
        ofDrawBitmapString("press p to Fluid", 700, 120);
        ofDrawBitmapString("press m to Circle", 700, 140);
        ofDrawBitmapString("press l to Line Fluid", 700, 160);
        ofDrawBitmapString("press k to Signal Up & Down", 700, 180);
        if (!lock) {
            ofSetColor(heetchRed);
            ofDrawBitmapString("!!! Press r to reset !!!", 700, 200);
            ofSetColor(pastelBlue1);
            ofDrawBitmapString("MIDI CONTROLS", 700, 220);
            ofDrawBitmapString("Press space to learn", 700, 240);
            ofDrawBitmapString("Press s to save", 700, 260);
            ofDrawBitmapString("Press $ to load", 700, 280);
            ofDrawBitmapString("Press backspace to unlearn", 700, 300);
            ofDrawBitmapString("Press 1 to set midi device 1", 700, 320);
            ofDrawBitmapString("Press 2 to set midi device 2", 700, 340);
            ofDrawBitmapString("Press - to list sound devices", 700, 360);
            ofDrawBitmapString("Press + to list midi devices", 700, 380);
        }
        
        masterGui.draw();
        waveFormGui.draw();
        signalGui.draw();
        signalUpDownGui.draw();
        fluidGui.draw();
        movingCircleGui.draw();
        lineFluidGui.draw();
        circlesGui.draw();
    }
    /*
    if (!lock) {
        if (soundDeviceList) {
            ofDrawBitmapString("Sound Device actuel : " + ofToString(storedSoundDeviceId), 250, 40);
            vector<ofSoundDevice> soundDevices = soundStream.getDeviceList();
            for (int i = 0; i < soundDevices.size(); i++) {
                ofDrawBitmapString(soundDevices[i].name + " [" + ofToString(soundDevices[i].deviceID) + "]", 250, 60 + i*20);
            }
        }
        
        if (midiDeviceList) {
            ofDrawBitmapString("Midi Device 1 actuel : " + ofToString(storedMidiDevice1Id), 250, 40);
            ofDrawBitmapString("Midi Device 2 actuel : " + ofToString(storedMidiDevice2Id), 250, 60);
            vector<string> midiDevices = ofxRtMidiIn::getPortList();
            for (int i = 0; i < midiDevices.size(); i++) {
                ofDrawBitmapString(midiDevices[i], 250, 80 + i*20);
            }
        }
        ofDrawBitmapString("SETTING MIDI 1", 250, ofGetHeight() - 180);
        sync1.drawDebug(250, ofGetHeight() - 160);
        ofDrawBitmapString("SETTING MIDI 2", 250, ofGetHeight() - 80);
        sync2.drawDebug(250, ofGetHeight() - 60);
        
        if (setMidi1) {
            ofDrawBitmapString("SETTING MIDI 1", 250, 20);
        }
        if (setMidi2) {
            ofDrawBitmapString("SETTING MIDI 2", 250, 20);
        }
        
        inputDeviceGui.draw();
        if (soundDeviceId != storedSoundDeviceId) {
            soundStream.close();
            soundStream.setDeviceID(soundDeviceId);
            soundStream.setup(this, 0, 2, 44100, bufferSize, 4);
            storedSoundDeviceId = soundDeviceId;
        }
        if (midiDevice1Id != storedMidiDevice1Id) {
            sync1.reset();
            if (midiDevice1Id == midiDevice2Id) midiDevice1Id++;
            sync1.setup(midiDevice1Id, allParameters);
            storedMidiDevice1Id = midiDevice1Id;
        }
        if (midiDevice2Id != storedMidiDevice2Id) {
            sync2.reset();
            if (midiDevice1Id == midiDevice2Id) midiDevice2Id++;
            sync2.setup(midiDevice2Id, allParameters);
            storedMidiDevice2Id = midiDevice2Id;
        }
    }
    */
    
}

//--------------------------------------------------------------
void ofApp::audioReceived(float * input, int bufferSize, int nChannels) {
    waveForm.audioReceived(input, bufferSize, nChannels);
    
    // Scale entre 0 et 1
    float maxValue = 0;
    for (int i = 0; i < bufferSize; i++) {
        if (abs(input[i]) > maxValue) {
            maxValue = abs(input[i]);
        }
    }
    for (int i = 0; i < bufferSize; i++) {
        input[i] /= maxValue;
    }
    
    
    // FFT
    fft->setSignal(input);
    
    float* curFft = fft->getAmplitude();
    memcpy(&audioBins[0], curFft, sizeof(float) * fft->getBinSize());
    
    float maxValueFft = 0;
    for(int i = 0; i < fft->getBinSize(); i++) {
        if(abs(audioBins[i]) > maxValueFft) {
            maxValueFft = abs(audioBins[i]);
        }
    }
    for(int i = 0; i < fft->getBinSize(); i++) {
        audioBins[i] /= maxValueFft;
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed  (int key){
    
    switch(key) {
        //VISUELS
        case 'u':
            caseKey=0;
            break;
        case 'i':
            caseKey=1;
            break;
        case 'o':
            caseKey=2;
            break;
        case 'p':
            caseKey=3;
            break;
        case 'm':
            caseKey=4;
            break;
        case 'l':
            caseKey=5;
            break;
        case 'k':
            caseKey=6;
            break;
        case 'y':
            caseKey=7;
            break;
        case 'a':
            caseKey = 8;
            break;
        
        //LOCK
        case '*':
            lock = !lock;
            if (lock) {
                soundDeviceList = false;
                midiDeviceList = false;
                setMidi1 = false;
                setMidi2 = false;
            }
            break;
            
        
        //RESET
        case 'r':
            if (!lock) {
                waveForm.setup();
                signal.setup();
                upDown.setup();
                signalUpDown.setup();
                fluid.setup();
                gradientCircle.setup();
                movingCircle.setup();
                lineFluid.setup();
            }
            break;
        /*
        //MIDI
        case '1':
            if (!lock) {
                setMidi1 = !setMidi1;
                setMidi2 = false;
            }
            break;
            
        case '2':
            if (!lock) {
                setMidi1 = false;
                setMidi2 = !setMidi2;
            }
            break;
            
        case ' ':
            if (setMidi1) {
                sync1.learn();
            }
            if (setMidi2) {
                sync2.learn();
            }
            break;
        case 's':
            if (setMidi1) {
                sync1.save("midi/midiSettings");
            }
            if (setMidi2) {
                sync2.save("midi/midiSettings");
            }
            break;
        case '$':
            if (setMidi1) {
                sync1.load("midi/midiSettings");
            }
            if (setMidi2) {
                sync2.load("midi/midiSettings");
            }
            break;
        case OF_KEY_BACKSPACE:
            if (setMidi1) {
                sync1.unlearn();
            }
            if (setMidi2) {
                sync2.unlearn();
            }
            break;
        */
        
        //HIDE GUI
        case 'h':
            hideGUI = !hideGUI;
            break;
            
        //LIST DEVICES
        case '-':
            if (!lock) {
                soundDeviceList = !soundDeviceList;
                midiDeviceList = false;
            }
            break;
        
        case '+':
            if (!lock) {
                midiDeviceList = !midiDeviceList;
                soundDeviceList = false;
            }
            break;
    }
}
