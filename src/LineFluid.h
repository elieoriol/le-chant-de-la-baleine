//
//  LineFluid.hpp
//  mySketch
//
//  Created by Michael Sedbon on 28/11/2016.
//
//

#ifndef LineFluid_h
#define LineFluid_h


#include "ofMain.h"
#include "ofxSyphon.h"


class LineFluid {
public:
    void setup();
    void draw();
    void audioReceived(float * input, int bufferSize, int nChannels = 2);
    
    ofParameterGroup Parameters;
    ofParameter<float> lowPct, highPct, speedCoef;
    ofParameter<int> height, waveNumber;
    ofParameter<int> nbNormals, normalLength;
    ofParameter<bool> normals;
    
    ofxSyphonServer OutputSyphonServer;
    
    int nbColorBands, storedNbColorBands;
    
    vector<ofColor> palette, setColors;
    ofColor fullWhite = ofColor::fromHex(0xFFFFFF);
    int colorMode, storedColorMode, alpha, storedAlpha, lineWidth;
};



#endif /* LineFluid_hpp */
