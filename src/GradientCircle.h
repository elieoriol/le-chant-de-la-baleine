//
//  GradientCircle.hpp
//  mySketch
//
//  Created by Michael Sedbon on 27/11/2016.
//
//

#ifndef GradientCircle_h
#define GradientCircle_h

#include "ofMain.h"
#include "ofxSyphon.h"



class GradientCircle {
public:
    void setup();
    void draw();
    
    ofParameterGroup Parameters;
    ofParameter<int> lineWidth;
    
    ofxSyphonServer OutputSyphonServer;
    
    int bufferSize = 256;
    
    vector <float> left, right, mainVolume;
    
    ofColor freshYellow = ofColor::fromHex(0xF1F2B5);
    ofColor orangeFire = ofColor::fromHex(0xEF473A);
    ofColor bleuVert = ofColor::fromHex(0x134E5E);
    ofColor vertDollar = ofColor::fromHex(0x71B280);
    ofColor blancCasse = ofColor::fromHex(0xECE9E6);
    ofColor pinotBleu = ofColor::fromHex(0x182848);
    ofColor magentaLove = ofColor::fromHex(0x89253E);
    ofColor greenLake = ofColor::fromHex(0x237A57);
    ofColor fullWhite = ofColor::fromHex(0xFFFFFF);
    
    
    ofColor clr1, clr2, clr3, clr4, clr5, clr6, clr7, clr8;
    
    int color;
    float time;
    
    
};
#endif /* GradientCircle_hpp */
