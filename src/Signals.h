//
//  Signal.h
//  mySketch 27_11_V2 2
//
//  Created by Taf on 30/11/2016.
//
//

#ifndef __mySketch_27_11_V2_2__Signal__
#define __mySketch_27_11_V2_2__Signal__

#include "ofMain.h"
#include "ofxSyphon.h"
#include "ofxFft.h"

#define MAX_PTS 500

class Signals {
public:
    void setup();
    void draw();
    
    ofParameterGroup Parameters;
    ofParameter<int> baseSpeed;
    ofParameter<float> coeffSpeed;
    ofParameter<bool> constantMode;
    ofParameter<int> constantSpeed;
    int mode, nbr, height;
    
    ofxSyphonServer OutputSyphonServer;
    
    int bufferSize = 256;
    
    
    ofPoint pts[MAX_PTS];
    int nPts;
    int storedMode, storedNbr;
    bool created;
    
    float lowAvg, threshold;
    
    int nbColorBands, storedNbColorBands;
    vector<ofColor> palette, setColors;
    ofColor fullWhite = ofColor::fromHex(0xFFFFFF);
    int colorMode, storedColorMode, alpha, storedAlpha, lineWidth;
};


#endif /* defined(__mySketch_27_11_V2_2__Signal__) */
